## Download the starter kit

## Install dependencies

cd my-app &&
npm i

## Run dev server

npm start

## Build

npm build
