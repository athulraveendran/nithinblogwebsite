import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'
import { QueryClient, QueryClientProvider } from 'react-query'
import { ReactQueryDevtools } from 'react-query/devtools'

import { Header } from './components/Header/Header'
import Home from './views/Home/Home'
import { NotFound } from './views/NotFound/NotFound'
import { DetailsPage } from './views/DetailsPage/DetailsPage'
import { About } from './views/About/About'

const queryClient = new QueryClient()
export const App = () => {
  return (
    <QueryClientProvider client={queryClient}>
      <ReactQueryDevtools initialIsOpen={false} />
      <Router>
        <Header />
        <Switch>
          <Route exact path="/">
            <Home />
          </Route>
          <Route exact path="/blog/:id">
            <DetailsPage />
          </Route>
          <Route exact path="/about">
            <About />
          </Route>
          <Route path="*" exact component={NotFound}></Route>
        </Switch>
      </Router>
    </QueryClientProvider>
  )
}
