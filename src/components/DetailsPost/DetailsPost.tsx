import React, { useEffect } from 'react'
import { useLocation, useParams } from 'react-router-dom'
import { UseQueryResult } from 'react-query'

import Regalix from '../../assets/Images/ThinkX11.jpg'
import { usePost } from '../../hooks/usePost'

import { Blog } from '../../types'

type ParmType = {
  id: string
}

export const DetailsPost = () => {
  const { pathname } = useLocation()
  const { id } = useParams<ParmType>()
  const { isLoading, data, isError }: UseQueryResult<Blog, Error> = usePost(id)

  useEffect(() => {
    window.scrollTo(0, 0)
  }, [pathname])

  return (
    <div className="mm">
      <>
        {isLoading ? (
          <span>Loading...</span>
        ) : isError ? (
          'Error'
        ) : (
          <div>
            <h2>{data?.title}</h2>
            <p>{data?.body}</p>
          </div>
        )}
      </>
      <div>
        <img src={Regalix} alt="regalix-img" className="details-img" />
      </div>
      <h2 className="default-font">The Most Powerful React JS Cheat Sheet</h2>
      <p className="default-font">
        React is a JavaScript library for building modern, powerful user
        interfaces. It was developed by Facebook engineers and introduced to the
        world in 2013 as an open source project. Today, it has emerged as the
        dominant frontend tool preferred by developers -- leading other
        libraries and frameworks in usage, adoption, developer experience, and
        interest.
      </p>
      <pre className="wp-block-code">
        <code>
          <p>
            {`
 import React from 'react';
 export const UserContext = React.createContext(
 export default function App() {
   return (
     <UserContext.Provider value="Reed">
       <User />
     </UserContext.Provider>
   )
 function User() {
   return (
     <UserContext.Consumer>
       {value => <h1>{value}</h1>} 
       {/* prints: Reed */}
     </UserContext.Consumer>
   )}`}
          </p>
        </code>
      </pre>
      <p className="default-font">
        Stay up to date. Learn from experienced engineers, maintainers, and core
        contributors who present what been working on the last year (or 2, or
        5). JavaScript and React are ever-evolving, and to fall behind these
        changes is to invite problems into your code and company. Gain
        perspective & insight. Conferences just about the talks -- you will also
        engage in conversations about the technology you love with engineers
        from other companies. They may help you to solve a probleme been having,
        or see something in a way that you and your company have never thought
        about. Get inspired. There is something magical about putting hundreds
        of like-minded folks -- each with their own unique modern, powerful user
        interfaces. It was developed by Facebook engineers and introduced to the
        world in 2013 as an open source project. Today, it has emerged as the
        dominant frontend tool preferred by developers -- leading other
        libraries and frameworks in usage, adoption, developer experience, and
        interest. React is a JavaScript library for building modern, powerful
        user interfaces. It was developed by Facebook engineers and introduced
        to the world in 2013 as an open source project. Today, it has emerged as
        the dominant frontend tool preferred by developers -- leading other
        libraries and frameworks in usage, adoption, developer experience, and
        interest.
      </p>
      <p className="default-font">
        React is a JavaScript library for building modern, powerful user
        interfaces. It was developed by Facebook engineers and introduced to the
        world in 2013 as an open source project. Today, it has emerged as the
        dominant frontend tool preferred by developers -- leading other
        libraries and frameworks in usage, adoption, developer experience, and
        interest.
      </p>
      <p className="default-font">
        React is a JavaScript library for building modern, powerful user
        interfaces. It was developed by Facebook engineers and introduced to the
        world in 2013 as an open source project. Today, it has emerged as the
        dominant frontend tool preferred by developers -- leading other
        libraries and frameworks in usage, adoption, developer experience, and
        interest.
      </p>
      <p className="default-font">
        React is a JavaScript library for building modern, powerful user
        interfaces. It was developed by Facebook engineers and introduced to the
        world in 2013 as an open source project. Today, it has emerged as the
        dominant frontend tool preferred by developers -- leading other
        libraries and frameworks in usage, adoption, developer experience, and
        interest.
      </p>
      <p className="default-font">
        React is a JavaScript library for building modern, powerful user
        interfaces. It was developed by Facebook engineers and introduced to the
        world in 2013 as an open source project. Today, it has emerged as the
        dominant frontend tool preferred by developers -- leading other
        libraries and frameworks in usage, adoption, developer experience, and
        interest.
      </p>
    </div>
  )
}
