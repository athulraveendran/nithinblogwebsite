import React from 'react'
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder'
import ChatBubbleOutlineIcon from '@mui/icons-material/ChatBubbleOutline'
import BookmarkBorderIcon from '@mui/icons-material/BookmarkBorder'
import MoreHorizIcon from '@mui/icons-material/MoreHoriz'
import ArrowBackIcon from '@mui/icons-material/ArrowBack'
import { useHistory } from 'react-router-dom'

import './detailsPost.css'

export const DetailsSideNav = () => {
  const history = useHistory()
  return (
    <div className="detailsSideNavContainer">
      <div
        className="back-arrow cursor-pointer"
        onClick={() => history.goBack()}
        aria-hidden="true"
      >
        <ArrowBackIcon />
      </div>

      <div className="sticky">
        <nav>
          <ul className="detailsSideNav-sub-container">
            <li className="detailsSideNav-item cursor-pointer">
              <span className="">
                <FavoriteBorderIcon />
              </span>
            </li>
            <li className="detailsSideNav-item cursor-pointer">
              <span className="">
                <ChatBubbleOutlineIcon />
              </span>
            </li>
            <li className="detailsSideNav-item cursor-pointer">
              <span>
                <BookmarkBorderIcon />
              </span>
            </li>
            <li className="detailsSideNav-item cursor-pointer">
              <span>
                <MoreHorizIcon />
              </span>
            </li>
          </ul>
        </nav>
      </div>
    </div>
  )
}
