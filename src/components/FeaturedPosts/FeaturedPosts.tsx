import React from 'react'

import './featuredPosts.css'
import SearchIcon from '@material-ui/icons/Search'
import itemImage from '../../assets/Images/ThinkX3.jpg'
import itemImagex from '../../assets/Images/ThinkX13.png'
import itemImagey from '../../assets/Images/ThinkX12.jpg'
import itemImagez from '../../assets/Images/ThinkX11.jpg'

export const FeaturedPosts = () => {
  return (
    <div className="cs-sidebar__inner">
      <div className="cs-search__container">
        <input className="cs-search__input" placeholder="search.."></input>
        <SearchIcon className="cs-search__submit" />
      </div>
      <div className="featured-main-container">
        <h3>Featured Post</h3>
        {/* start */}
        <div className="featured-container flex-row">
          <div className="featured-image-container flex-colum-center">
            <img
              className="featured-image"
              src={itemImage}
              alt="featured post"
            ></img>
          </div>
          <div>
            <h5>React Context for Beginners – The Complete Guide (2021)</h5>
            <p>
              <span className="featured-date">July 21 2021</span>
            </p>
          </div>
        </div>
        {/* stop */}
        <div className="featured-container flex-row">
          <div className="featured-image-container flex-colum-center">
            <img
              className="featured-image"
              src={itemImagex}
              alt="featured post"
            ></img>
          </div>
          <div>
            <h5>React Context for Beginners – The Complete Guide (2021)</h5>
            <p>
              <span className="featured-date">July 21 2021</span>
            </p>
          </div>
        </div>
        <div className="featured-container flex-row">
          <div className="featured-image-container flex-colum-center">
            <img
              className="featured-image"
              src={itemImagey}
              alt="featured post"
            ></img>
          </div>
          <div>
            <h5>React Context for Beginners – The Complete Guide (2021)</h5>
            <p>
              <span className="featured-date">July 21 2021</span>
            </p>
          </div>
        </div>
        <div className="featured-container flex-row">
          <div className="featured-image-container flex-colum-center">
            <img
              className="featured-image"
              src={itemImagez}
              alt="featured post"
            ></img>
          </div>
          <div>
            <h5>React Context for Beginners – The Complete Guide (2021)</h5>
            <p>
              <span className="featured-date">July 21 2021</span>
            </p>
          </div>
        </div>
      </div>
    </div>
  )
}
