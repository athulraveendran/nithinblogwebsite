import React from 'react'
import FacebookIcon from '@material-ui/icons/Facebook'
import TwitterIcon from '@material-ui/icons/Twitter'
import LinkedInIcon from '@material-ui/icons/LinkedIn'
import PinterestIcon from '@material-ui/icons/Pinterest'

import './footer.css'

export const Footer = () => {
  return (
    <footer className="crayons-footer default-font ">
      <div className="crayons-layout crayons-layout--limited-m crayons-footer__container">
        <div>
          <p className="footer__description">
            <span className="Z-link">Z Arrow</span>– A constructive and
            inclusive social network for software developers. With you every
            step of your journey.
          </p>
        </div>
        <div>
          <h5>Follow US On Socials</h5>
          <ul className="social-share-container flex-row-center">
            <li className="social-share-item">
              <FacebookIcon />
            </li>
            <li className="social-share-item">
              <TwitterIcon />
            </li>
            <li className="social-share-item">
              <LinkedInIcon />
            </li>
            <li className="social-share-item">
              <PinterestIcon />
            </li>
          </ul>
        </div>
      </div>
    </footer>
  )
}
