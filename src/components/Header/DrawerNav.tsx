import React from 'react'
import {
  Close,
  Facebook,
  Twitter,
  LinkedIn,
  Pinterest,
} from '@material-ui/icons'

import './drawer.css'

interface HeaderProps {
  theme: boolean
  toggleMenu: React.MouseEventHandler<SVGSVGElement> | undefined
}

export const DrawerNav = (props: HeaderProps) => {
  return (
    <div className="drawer-main-container">
      <div
        className={
          props.theme
            ? 'bg-dark drawer-sub-container'
            : ' bg-light drawer-sub-container'
        }
      >
        <div className="close-button-container">
          <Close className="close-button" onClick={props.toggleMenu} />
        </div>
        {/* <div className="drawer-cmy-name">Z Arrow Icon</div> */}
        <h2 className="drawer-cmy-name">Z Arrow Icon</h2>
        <ul className="drawer-nav-items-container">
          <h3 className="drawer-categories-head">Categories</h3>
          <li className="drawer-nav-items">React Js</li>
          <li className="drawer-nav-items">Angular </li>
          <li className="drawer-nav-items">Node</li>
          <li className="drawer-nav-items">HTML</li>
          <li className="drawer-nav-items">JavaScript</li>
        </ul>
        <div className=" section-space--mt_60">
          <h4>Contact Us</h4>
          <p className="mobile-menu-contact-info">
            Minato-ku, Ny 10002, Tokyo, Japan <br />
            support.center@unero.co <br />
            (0091) 8547 632521
          </p>
        </div>
        <div>
          <h5>Follow US On Socials</h5>
          <ul className="social-share">
            <li className="social-share-item">
              <Facebook />
            </li>
            <li className="social-share-item">
              <Twitter />
            </li>
            <li className="social-share-item">
              <LinkedIn />
            </li>
            <li className="social-share-item">
              <Pinterest />
            </li>
          </ul>
        </div>
      </div>
    </div>
  )
}
