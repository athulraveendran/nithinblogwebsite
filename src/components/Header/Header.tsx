import React, { useState } from 'react'
import { Menu, NightsStay, Search } from '@material-ui/icons'
import { Switch, FormControlLabel } from '@material-ui/core'
import { ThemeProvider } from 'styled-components'
import { NavLink } from 'react-router-dom'
import { useSelector, useDispatch } from 'react-redux'
import { bindActionCreators } from 'redux'

import { actionCreators } from '../../reduxState'
import './header.css'
import { GlobalStyles } from '../../styles/globalStyles'
import { lightTheme, darkTheme } from '../../styles/Themes'
import { DrawerNav } from './DrawerNav'
import { SearchBar } from '../SearchBar/SearchBar'
import { RootState } from '../../reduxState/reducers'

export const Header = () => {
  const state = useSelector((state: RootState) => state.theme)
  const dispatch = useDispatch()
  const { changeTheme } = bindActionCreators(actionCreators, dispatch)
  const [menubar, setMenuBar] = useState(false)
  const [searchToggle, setSearchToggle] = useState(false)

  const toggleMenu = () => setMenuBar((prev) => !prev)

  const handleSearchToggle = () => setSearchToggle((prev) => !prev)
  return (
    <ThemeProvider theme={state.theme ? darkTheme : lightTheme}>
      <GlobalStyles />
      {menubar ? (
        <DrawerNav theme={state.theme} toggleMenu={toggleMenu} />
      ) : null}
      <div
        className={state.theme ? 'main-header bg-dark' : 'main-header bg-light'}
      >
        <header className="flex-row header-style" data-scheme="default">
          <div className="header-left-container flex-row">
            <div className="flex-row">
              <span className="menu-icon flex-colum-center nav-padding-right cursor-pointer">
                <Menu onClick={toggleMenu} />
              </span>
              <span className="flex-colum-center cmy-name-container">
                <p className="default-font cmy-name-text cursor-pointer">
                  Z Arrow
                </p>
              </span>
            </div>

            <div className="header-left-second-container">
              <nav>
                <ul className="flex-row default-font">
                  <li className="">
                    <span className="nav-name cursor-pointer nav-bar-font nav-padding-right">
                      Topic
                    </span>
                    <div></div>
                  </li>
                  <li className="">
                    <span className="nav-name cursor-pointer nav-bar-font nav-padding-right">
                      BootCamp
                    </span>
                    <div></div>
                  </li>
                  <li>
                    <span className=" nav-bar-font cursor-pointer nav-padding-right">
                      <NavLink
                        className={state.theme ? 'color-light' : '  color-dark'}
                        to="/about"
                      >
                        Sign In
                      </NavLink>
                    </span>
                  </li>
                </ul>
              </nav>
            </div>
          </div>
          <div className="header-right-continer flex-colum-center">
            <div className="flex-row">
              <div className="nightmod-container flex-row">
                <span className="nightmode-swich-button cursor-pointer flex-colum-center nav-padding-left nav-padding-right">
                  <FormControlLabel
                    control={
                      <Switch size="small" onClick={() => changeTheme()} />
                    }
                    label={undefined}
                  />
                </span>
                <span className="mobile-nightmode-swich-button cursor-pointer">
                  <NightsStay onClick={() => changeTheme()} />
                </span>
              </div>
              <div className="cursor-pointer flex-colum-center nav-padding-left">
                <Search onClick={handleSearchToggle} />
              </div>
            </div>
          </div>
        </header>
      </div>
      {searchToggle ? (
        <div
          className={
            state.theme
              ? 'search-bar-container bg-dark'
              : 'search-bar-container bg-light'
          }
        >
          <SearchBar handleSearchToggle={handleSearchToggle} />
        </div>
      ) : null}
    </ThemeProvider>
  )
}
