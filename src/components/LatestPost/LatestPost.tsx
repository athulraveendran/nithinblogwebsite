import { useSelector } from 'react-redux'
import InfiniteScroll from 'react-infinite-scroller'
import CircularProgress from '@mui/material/CircularProgress'
import Box from '@mui/material/Box'
import { NavLink } from 'react-router-dom'

import { RootState } from '../../reduxState/reducers'
import './latestPost.css'
import { usePosts } from '../../hooks/usePosts'

export const LatestPost = () => {
  const state = useSelector((state: RootState) => state.theme)
  const { data, isLoading, isError, hasNextPage, fetchNextPage } = usePosts()

  return (
    <div>
      <div>
        <h2>Latest Post</h2>
      </div>

      <div className="latest-item-main-container">
        {isLoading ? (
          <div className="flex-row-center">
            <Box>
              <CircularProgress color="inherit" />
            </Box>
          </div>
        ) : isError ? (
          <p>There was an error</p>
        ) : (
          <InfiniteScroll hasMore={hasNextPage} loadMore={fetchNextPage}>
            {data
              ? data.pages.map((page: any) =>
                  page.results.map((item: any) => (
                    <NavLink to={`/blog/${item.id}`} key={item.id}>
                      <button
                        className={
                          state.theme
                            ? 'latest-item-sub-container color-light'
                            : 'latest-item-sub-container color-dark'
                        }
                      >
                        <div>
                          <div className="flex-row mobile-view-latest-post-continer">
                            <div className="item-image-container flex-colum-center">
                              <img
                                className="item-image"
                                src={item.download_url}
                                alt="content-img"
                              ></img>
                            </div>
                            <div className="flex-colum position-relative">
                              <h6 className="position-absolute latest-post-item-top latest-item-commen">
                                {item.author}
                              </h6>{' '}
                              <h3 className="latest-item-commen latest-item-h">
                                React Context for Beginners – The Complete Guide
                                (2021)
                              </h3>
                              <p className="latest-item-commen latest-item-p">
                                React context is an essential tool for every
                                React developer to know. I lets you easily share
                                state…{' '}
                              </p>
                              <div className="latest-item-commen position-absolute latest-post-item-bottom">
                                <span className="latest-post-by">
                                  By Beed Bager <span>July 21, 2021</span>
                                </span>
                              </div>
                            </div>
                          </div>
                        </div>
                      </button>
                    </NavLink>
                  ))
                )
              : 'Loading.....'}
          </InfiniteScroll>
        )}
      </div>
    </div>
  )
}
