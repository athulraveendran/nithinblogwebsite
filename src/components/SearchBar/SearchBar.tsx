import React from 'react'
import { Search, Close } from '@material-ui/icons'

import './searchbar.css'

interface HeaderProps {
  handleSearchToggle: React.MouseEventHandler<SVGSVGElement> | undefined
}

export const SearchBar = (props: HeaderProps) => {
  return (
    <div className="cs-container">
      <form className="cs-search__nav-form">
        <div className="cs-search__group">
          <button className="cs-search_submit">
            <Search className="cs-search_submit search-icon" />
          </button>
          <input className="cs-search_input" placeholder="Search..."></input>
          <button className="cs-search__close">
            <Close onClick={props.handleSearchToggle} />
          </button>
        </div>
      </form>
    </div>
  )
}
