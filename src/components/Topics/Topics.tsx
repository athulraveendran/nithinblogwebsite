import React from 'react'

import './topics.css'

export const Topics = () => {
  return (
    <>
      <div className="widget powerkit_widget_featured_categories-3 powerkit_widget_featured_categories">
        <h4 className="cs-section-heading cnvs-block-section-heading is-style-cnvs-block-section-heading-default halignleft  ">
          <span className="cnvs-section-title">
            <span>Topics</span>
          </span>
        </h4>
        <div className="widget-body">
          <div className="pk-featured-categories pk-featured-categories-vertical-list">
            <div className="pk-featured-item">
              <div className="pk-featured-content">
                <div className="pk-featured-inner">
                  <div className="pk-featured-name">
                    <p>React</p>
                  </div>

                  <div className="pk-featured-count">
                    <span className="pk-featured-number">38</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="pk-featured-item">
              <div className="pk-featured-content">
                <div className="pk-featured-inner">
                  <div className="pk-featured-name">
                    <p>Node </p>
                  </div>

                  <div className="pk-featured-count">
                    <span className="pk-featured-number">2</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="pk-featured-item">
              <div className="pk-featured-content">
                <div className="pk-featured-inner">
                  <div className="pk-featured-name">
                    <p>JavaScript</p>{' '}
                  </div>

                  <div className="pk-featured-count">
                    <span className="pk-featured-number">2</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="pk-featured-item">
              <div className="pk-featured-content">
                <div className="pk-featured-inner">
                  <div className="pk-featured-name">
                    <p>GraphQL</p>{' '}
                  </div>

                  <div className="pk-featured-count">
                    <span className="pk-featured-number">1</span>
                  </div>
                </div>
              </div>
            </div>
            <div className="pk-featured-item">
              <div className="pk-featured-content">
                <div className="pk-featured-inner">
                  <div className="pk-featured-name">
                    <p>CSS</p>{' '}
                  </div>

                  <div className="pk-featured-count">
                    <span className="pk-featured-number">1</span>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}
