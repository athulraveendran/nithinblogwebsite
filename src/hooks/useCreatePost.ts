import { useMutation, useQueryClient } from 'react-query'

import { Blog } from '../types'

interface IcreatePost {
  title: string
  body: string
  userId: number
}
interface Icontext {
  previousItem: Blog | undefined
}

export const createPost = async (
  title: string,
  body: string,
  userId: number
): Promise<Blog> => {
  const res: Response = await fetch(
    `https://jsonplaceholder.typicode.com/posts`,
    {
      method: 'POST',
      body: JSON.stringify({
        title,
        body,
        userId,
      }),
      headers: {
        'Content-type': 'application/json; charset=UTF-8',
      },
    }
  )
  if (res.ok) {
    return res.json()
  }
  throw new Error('Error create person')
}
export const useCreatePost = () => {
  const queryClient = useQueryClient()
  return useMutation<Blog, Error, IcreatePost>(
    async ({ title, body, userId }) => createPost(title, body, userId),
    {
      onMutate: async (variables: IcreatePost) => {
        // A mutation is about to happen!
        // Optionally return a context containing data to use when for example rolling back
        console.log('onMutate event', variables)
        await queryClient.cancelQueries('test')
        let previousItem: Blog | undefined
        const newItem: Blog = {
          id: Math.random(),
          body: variables.body,
          title: variables.title,
          userId: variables.userId,
        }
        queryClient.setQueryData('test', (old: any) => [...old, newItem])
        return { previousItem }
      },
      onSuccess: (
        data: Blog,
        _variables: IcreatePost,
        _context: any | Icontext
      ) => {
        // success api post data
        console.log('onSuccess event ', data, _variables, _context)
        return console.log(`rolling back with updated id: ${data}`)
      },
      onError: (
        error: Error,
        _variables: IcreatePost,
        context: any | Icontext
      ) => {
        // An error happened!
        queryClient.setQueryData('test', context?.previousItem)
        console.log('onError event', error, _variables, context)
        console.log(`rolling back optimistic update with id ${context?.id}`)
      },

      onSettled: (
        _data: Blog | undefined,
        _error: Error | null,
        _variables: IcreatePost | undefined,
        _context: any | Icontext
      ) => {
        // this is like finally in try catch this will get exicute after every thing
        console.log('onSettled event', _data, _error, _variables, _context)
        return console.log(`complete mutaion`)
      },
    }
  )
}
