import { useQuery } from 'react-query'

import { Blog } from '../types'

export const fetchPost = async (postId: string): Promise<Blog> => {
  try {
    const response = await fetch(
      `https://jsonplaceholder.typicode.com/posts/${postId}`
    )
    if (!response.ok) {
      throw new Error('Problem fetching data')
    }
    const results = await response.json()
    return results
  } catch (error) {
    throw new Error('error fetching blog with id')
  }
}
export const usePost = (postId: string) => {
  return useQuery<Blog, Error>(['posts', postId], () => fetchPost(postId), {
    enabled: !!postId,
  })
}
