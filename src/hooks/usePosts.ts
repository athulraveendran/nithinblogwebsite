import { useInfiniteQuery } from 'react-query'

import { BlogItem, nextPage, totalPages } from '../types'

export interface BlogInfiniteQueryType {
  results: BlogItem
  nextPage: nextPage
  totalPages: totalPages
}

const fetchPosts = async ({
  pageParam = 1,
}): Promise<BlogInfiniteQueryType> => {
  try {
    const response = await fetch(
      `https://picsum.photos/v2/list?page=${pageParam}&limit=10`
    )
    if (!response.ok) {
      throw new Error('Problem fetching data')
    }
    const results = await response.json()

    return { results, nextPage: pageParam + 1, totalPages: 100 }
  } catch (error) {
    throw new Error('Problem fetching data')
  }
}

export const usePosts = () => {
  return useInfiniteQuery<BlogInfiniteQueryType, Error>('posts', fetchPosts, {
    getNextPageParam: (lastPage: BlogInfiniteQueryType) => {
      if (lastPage.nextPage < lastPage.totalPages) return lastPage.nextPage
      return undefined
    },
  })
}
