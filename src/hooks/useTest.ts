import { useQuery } from 'react-query'

import { Blog } from '../types'

export const fetchTest = async (): Promise<Blog[]> => {
  try {
    const response = await fetch(`https://jsonplaceholder.typicode.com/posts`)
    if (!response.ok) {
      throw new Error('Problem fetching data')
    }
    const results = await response.json()
    return results
  } catch (error) {
    throw new Error('error fetching blog with id')
  }
}
export const useTest = () => {
  return useQuery<Blog[], Error>(['test'], fetchTest)
}
