import { Dispatch } from 'redux'
import { ActionType } from '../action-types'
import { ActionTheme, ActionUser } from '../actions/index'

export const itemsFetchData = () => {
  return (dispatch: Dispatch<ActionUser>) => {
    const fetchApiCall = async () => {
      try {
        const endPoint = `https://fakestoreapi.com/products/`
        const response = await (
          await fetch(endPoint, {
            method: 'get',
          })
        ).json()
        dispatch({
          type: ActionType.USERAPI,
          payload: response,
        })
      } catch (error) {
        console.log('Error Happend', error)
      }
    }
    fetchApiCall()
  }
}

export const changeTheme = () => {
  return (dispatch: Dispatch<ActionTheme>) => {
    dispatch({
      type: ActionType.THEMECHANGE,
    })
  }
}
