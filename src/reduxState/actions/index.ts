import { ActionType } from '../action-types/index'

interface UserApi {
  type: ActionType.USERAPI
  payload: any
}
interface THEMECHANGE {
  type: ActionType.THEMECHANGE
}

export type ActionUser = UserApi
export type ActionTheme = THEMECHANGE
