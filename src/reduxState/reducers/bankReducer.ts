import { ActionType } from '../action-types/index'
import { ActionUser } from '../actions'

interface intialStateType {
  result: any
}
const initialState = {
  result: [],
}

const reducer = (state: intialStateType = initialState, action: ActionUser) => {
  switch (action.type) {
    case ActionType.USERAPI:
      return {
        ...state,
        result: action.payload.result,
      }
    default:
      return state
  }
}

export default reducer
