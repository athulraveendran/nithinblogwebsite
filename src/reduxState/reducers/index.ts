import { combineReducers } from 'redux'
import bankReducer from './bankReducer'
import themeReducer from './themeReducer'

const reducers = combineReducers({
  bank: bankReducer,
  theme: themeReducer,
})

export default reducers

export type RootState = ReturnType<typeof reducers>
