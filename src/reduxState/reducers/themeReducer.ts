import { ActionType } from '../action-types/index'
import { ActionTheme } from '../actions'

interface intialStateType {
  theme: boolean
}
const initialState = {
  theme: false,
}

const themeReducer = (
  state: intialStateType = initialState,
  action: ActionTheme
) => {
  switch (action.type) {
    case ActionType.THEMECHANGE:
      return {
        ...state,
        theme: !state.theme,
      }
    default:
      return state
  }
}

export default themeReducer
