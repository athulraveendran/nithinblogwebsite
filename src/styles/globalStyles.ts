import { createGlobalStyle } from 'styled-components'

interface GlobalStyleProps {
  theme: {
    body: string
    text: string
    toggleBorder: string
    background: string
  }
}

export const GlobalStyles = createGlobalStyle<GlobalStyleProps>`
  body {
    background: ${({ theme }) => theme.body};
    color: ${({ theme }) => theme.text};
    font-family: Tahoma, Helvetica, Arial, Roboto, sans-serif;
    transition: all 0.50s linear;
  }
  `
