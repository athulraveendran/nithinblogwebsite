export interface Blog {
  userId: number
  id: number
  title: string
  body: string
}

export interface BlogItem {
  author: string
  download_url: string
  height: number
  id: string
  url: string
  width: number
}
export type nextPage = number
export type totalPages = number
