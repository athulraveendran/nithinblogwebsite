import React from 'react'

import { useTest } from '../../hooks/useTest'
import { useCreatePost } from '../../hooks/useCreatePost'

export const About = () => {
  const { isLoading, data, isError } = useTest()
  const createPost = useCreatePost()

  const onSubmitData = () =>
    createPost.mutate({
      title: 'foo',
      body: 'bar',
      userId: 1,
    })

  if (isError) <p>some error happed</p>
  return (
    <div style={{ margin: '20px' }}>
      {isLoading ? <p>loding....</p> : null}
      {data ? data.map((item: any) => <p key={item.id}>{item.title}</p>) : ''}
      <button onClick={onSubmitData}>Submit</button>
    </div>
  )
}
