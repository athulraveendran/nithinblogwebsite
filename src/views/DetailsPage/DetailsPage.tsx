import React from 'react'

import { DetailsSideNav } from '../../components/DetailsPost/DetailsSideNav'
import { Footer } from '../../components/Footer/Footer'
import { DetailsPost } from '../../components/DetailsPost/DetailsPost'
import Ads from '../../components/DetailsPost/Ads'
import './detailsPage.css'

export const DetailsPage = () => {
  return (
    <div className="position-relative m">
      <div className="flex-row">
        <DetailsSideNav />
        <DetailsPost />
        <div className="ads-container">
          <Ads />
        </div>
      </div>
      <Footer />
    </div>
  )
}
