import React, { useState, useEffect } from 'react'

import { LatestPost } from '../../components/LatestPost/LatestPost'
import { FeaturedPosts } from '../../components//FeaturedPosts/FeaturedPosts'
import { Topics } from '../../components//Topics/Topics'
import ExpandLessIcon from '@material-ui/icons/ExpandLess'
import { Footer } from '../../components//Footer/Footer'
import './home.css'

export default function Main() {
  const [isVisible, setIsVisible] = useState(false)

  // Top: 0 takes us all the way back to the top of the page
  // Behavior: smooth keeps it smooth!
  const scrollToTop = () => {
    window.scrollTo({
      top: 0,
      behavior: 'smooth',
    })
  }

  useEffect(() => {
    // Button is displayed after scrolling for 500 pixels
    const toggleVisibility = () => {
      if (window.pageYOffset > 200) {
        setIsVisible(true)
      } else {
        setIsVisible(false)
      }
    }

    window.addEventListener('scroll', toggleVisibility)

    return () => window.removeEventListener('scroll', toggleVisibility)
  }, [])

  return (
    <div className="mainy-container">
      <div className="mainx-container">
        <div className="latestPost-containr">
          <LatestPost />
        </div>
        <div className="FeaturedPosts-topics-container ">
          <FeaturedPosts />
          <div className="sticky">
            <Topics />
          </div>
        </div>
      </div>
      <div>
        <Footer />
      </div>
      <div className="scroll-to-top-container">
        {isVisible && (
          <ExpandLessIcon className="scroll-to-top" onClick={scrollToTop} />
        )}
      </div>
    </div>
  )
}
