import React from 'react'

import urlErrorImg from '../../assets/Images/broken-img-2.png'
import './notFound.css'

export const NotFound = () => {
  return (
    <div className="urlNotFound-container">
      <div className="urlNotFound">
        <img className="url-img" src={urlErrorImg} alt="broken-img"></img>
        <p> The page You are looking Not found.Please check your URL</p>
      </div>
    </div>
  )
}
